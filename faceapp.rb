# faceapp.rb
require 'rubygems'
require 'bundler'
Bundler.require
require 'sinatra'
require 'aws-sdk'
require 'sinatra/partial'
require 'open-uri'
require 'base64'
require 'firebase'

# Load up all AWS or other keys
Dotenv.load

# Set up our AWS authentication for all calls in this app
Aws.config.update({
        :region => 'us-east-1',
        :credentials => Aws::Credentials.new(ENV['AWS_KEY'],ENV['AWS_SECRET'])
    })


# Set up firewase authentication for all calls in this app
firebase = Firebase::Client.new(ENV['FIREBASE_URL'], ENV['FIREBASE_SECRET'])


# Default collection name
FACE_COLLECTION = "faceapp_test"
FACE_TABLE = 'facialRekognition'


#Changer le moteur de template par defaut
set :partial_template_engine, :erb


# The routes
get '/' do
  # Show the main index page
  erb :faceapp
end

get '/log' do
  # gestion de collection d'images
  erb :log
end

get '/manage' do
  # page administration
  erb :manage
end


post '/upload/:guid/:photoid' do
  client = Aws::Rekognition::Client.new()
  response = client.index_faces({
    collection_id: FACE_COLLECTION,
    external_image_id: params[:guid],
    image: {
      bytes: request.body.read.to_s
    }
  })


  begin
    result = firebase.push("facialReko", 
          {
            'id' => params[:guid].to_s,
            'username' => params[:photoid].to_s,
            params[:guid].to_s => params[:photoid].to_s,
          }
      )

    p 'Added user: ' + params[:guid] + ' - ' + params[:photoid]
    "Image uploaded safely!"
  rescue  Exception => error
    p 'Unable to add user:'
    p error.message
  end
  
end


post '/compare' do
  content_type :json

  begin
    client = Aws::Rekognition::Client.new()
    response = client.search_faces_by_image({
      collection_id: FACE_COLLECTION,
      max_faces: 1,
      face_match_threshold: 95,
      image: {
        bytes: request.body.read.to_s
      }
    })

    if response.face_matches.count > 1
      {:message => "Trop de visages"}.to_json
    elsif response.face_matches.count == 0
      {:message => "Aucun visage présent!"}.to_json
    else
      # "Comparison finished - detected #{ response.face_matches[0].face.external_image_id } with #{ response.face_matches[0].face.confidence } accuracy."
      begin
        faceImgId = response.face_matches[0].face.external_image_id
        record = firebase.get('facialReko', { 'id' => faceImgId })
        
        #resp = record.body.find_all_values_for(faceImgId)
        resp = record.body.map{|t,th|th}.flatten.first

        usernameFound = ''

        #p "resp", resp

        if resp.nil?
          {:message => "Utilisateur non identifié."}.to_json        
        else

          if !resp['username'].nil?
            usernameFound = resp['username']
          else
            if !resp[0].nil?
              usernameFound = resp[0]
            end
          end

            {:username => usernameFound, :id => faceImgId, :confidence => response.face_matches[0].face.confidence, :message => "Utilisateur"}.to_json
        end
      rescue Exception => error
        p 'Utilisateur introuvable:'
        p error.message
      end
    end

  rescue Aws::Rekognition::Errors::ServiceError => error
    p error.message
    {:message => "Erreur! Positionnez-vous bien et réessayez"}.to_json
  end

end


post '/speech' do
  client = Aws::Polly::Client.new()
  response = client.synthesize_speech({
    output_format: "mp3",
    voice_id: "Lea",
    text: params[:tosay]
  })
  Base64.encode64(response.audio_stream.string)
end


post '/log' do
  filename = "#{settings.root}/public/log/" + params[:namePicture]+ " " + params[:dateLog] + ".png"

  data =  params[:image_url]
  image_data = Base64.decode64(data['data:image/png;base64,'.length .. -1])
  File.open(filename, 'wb') do |f|
    f.write image_data
  end

  #p request.body.read.to_s
  #p params[:namePicture], params[:dateLog]
  {:message => "Erreur! Positionnez-vous bien et réessayez"}.to_json
  "ok"
end



=begin
get '/collection/:action' do
  client = Aws::Rekognition::Client.new()
  collections = client.list_collections({}).collection_ids
  case params[:action]
    when 'create'
      if !(collections.include? FACE_COLLECTION)
        response = client.create_collection({ collection_id: FACE_COLLECTION })
      end
    when 'delete'
      if (collections.include? FACE_COLLECTION)
        response = client.delete_collection({ collection_id: FACE_COLLECTION })
      end
  end
  redirect '/'
end
=end



class Hash
  def find_all_values_for(key)
    result = []
    result << self[key]
    self.values.each do |hash_value|
      values = [hash_value] unless hash_value.is_a? Array
      # values = [hash_value.to_s] if hash_value.is_a? String
      values.each do |value|
        result += value.find_all_values_for(key) if value.is_a? Hash
      end
    end
    result.compact
  end
end