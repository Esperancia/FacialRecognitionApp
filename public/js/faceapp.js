$(document).ready(function() {

  if (window.JpegCamera) {

    var camera; // placeholder

    // Add the photo taken to the current Rekognition collection for later comparison
    var add_to_collection = function() {
      var photo_id = $("#photo_id").val();
      if (!photo_id.length) {
        $("#upload_status").html("Please provide name for the upload");
        return;
      }
      var snapshot = camera.capture();
      var api_url = "/upload/" + guid() + "/" + photo_id;

      $("#loading_img").show();
      snapshot.upload({api_url: api_url}).done(function(response) {
        $("#upload_result").html(response);
        $("#loading_img").hide();
        this.discard();
      }).fail(function(status_code, error_message, response) {
        $("#upload_status").html("Upload failed with status " + status_code + " (" + error_message + ")");
        $("#upload_result").html(response);
        $("#loading_img").hide();
      });
    };


    // Compare the photographed image to the current Rekognition collection
    var compare_image = function() {
      var snapshot = camera.capture();
      var namePicture = "inconnu";

      snapshot.show();  //Faire une pause sur l'image capturée.

      var api_url = "/compare";
      $("#loading_img").show();
      //$("#camera").addClass("scanning");

      //$.when().then();

      snapshot.upload({api_url: api_url}).done(function(response) {
        var data = JSON.parse(response);
        
        if (data.id !== undefined && data.username != null) {
          $("#compare_image").hide();
          console.log(data);

          $("#upload_result").html(data.message + ": " + data.username + "<br> %: " + data.confidence);
            namePicture = data.username;
          // create speech response
          $.post("/speech", {tosay: greetingTime(moment()) + " " + data.username}, function(response) {
            $("#audio_speech").attr("src", "data:audio/mpeg;base64," + response);
            $("#audio_speech")[0].play();
          });
        } else {
          //alert(data.message);
          //$("#upload_result").html(data.message);
          swal({
                position: "top-end",
                title: "Accès refusé!", 
                text: data.message, 
                type: "error",
                timer: 1500
              }); 
        }

        $("#loading_img").hide();
        //$("#camera").removeClass("scanning");

        writeLog(snapshot, namePicture);
        this.discard();
      }).fail(function(status_code, error_message, response) {
        $("#upload_status").html("Upload failed with status " + status_code + " (" + error_message + ")");
        $("#upload_result").html(response);
        $("#loading_img").hide();
        writeLog(snapshot, namePicture);
      });
      
    };

    var greetingTime = function(moment) {
      var greet = null;
      
      if(!moment || !moment.isValid()) { return; } //if we can't find a valid or filled moment, we return.
            var split_afternoon = 12 //24hr time to split the afternoon
      var split_evening = 17 //24hr time to split the evening
      var currentHour = parseFloat(moment.format("HH"));
      
      if(currentHour >= split_afternoon && currentHour <= split_evening) {
        greet = "bonsoir"; //afternoon
      } else if(currentHour >= split_evening) {
        greet = "bonsoir"; //evening
      } else {
        greet = "bonjour"; //morning
      }      
      return greet;
    }

    // Define what the button clicks do.
    $("#add_to_collection").click(add_to_collection);
    $("#compare_image").click(compare_image);

    $(".log_item a").click(function(e){
      e.preventDefault();
      e.stopPropagation()
      swal({
        title: $(this).find('img').attr('src').split("/").pop().split(".")[0],
        imageUrl: $(this).find('img').attr('src'),
        imageHeight: 400,
        imageAlt: 'Accès ou tentative' + '',
      });
    });

    // Initiate the camera widget on screen
    var options = {
      shutter_ogg_url: "js/jpeg_camera/shutter.ogg",
      shutter_mp3_url: "js/jpeg_camera/shutter.mp3",
      swf_url: "js/jpeg_camera/jpeg_camera.swf"
    }
    

    camera = new JpegCamera("#camera", options).ready(function(info) {
      $("#loading_img").hide();
    });


    var writeLog = function(snapshot, namePicture, dateLog) {
      namePicture = namePicture || "inconnu";
      dateLog = dateLog || moment(new Date()).locale('fr').format('LLLL');

      var api_url = "/log";

      if (JpegCamera.canvas_supported()) {

        var canvas = snapshot._canvas;
        var mime = mime || "image/png";
        
        if (camera.options.mirror) {
          canvas = get_flipped_canvas(canvas);
        }

        $.post(api_url, {"namePicture": namePicture, "dateLog": dateLog, "image_url": canvas.toDataURL(mime)}, function(response) {
          console.log("log written...");
        });
        
      } else {
        swal({
            position: "top-end",
            title: "Erreur!", 
            text: "Canvas non supporté", 
            type: "error"
          }); 
      }

    } //fin writeLog


  }


  var guid = function() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }


});
